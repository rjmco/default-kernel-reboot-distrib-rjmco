%define	cron_job_order	99

Name:		default-kernel-reboot
Version:	0.2
Release:	4%{?dist}
Summary:	Reboots the system if the running kernel is different then GRUB's default

Group:		System Environment/Base
License:	GPLv3+
URL:		https://bitbucket.org/rjmco/%{name}
Source0:	https://bitbucket.org/rjmco/%{name}/downloads/%{name}-%{version}.tar.gz

BuildRoot:	%{_topdir}/BUILDROOT
BuildArchitectures:	noarch

Requires:	crontabs
Requires:	upstart
Requires:	grub
Requires:	util-linux-ng

%description
Install this package if you want to reboot the system when the running kernel is
different then the one configured as the default in GRUB

This package installs a cron job at /etc/cron.d/%{cron_job_order}default-kernel-reboot. You
will need to enable it.


%prep
%setup -q

%build
echo OK


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p %{buildroot}%{_sysconfdir}/cron.d/
mkdir -p %{buildroot}%{_libexecdir}/
%{__install} -p -D -m 0644 %{name}.cron %{buildroot}%{_sysconfdir}/cron.d/%{cron_job_order}%{name}
if [[ "%{dist}" == ".el6" ]]; then
	%{__install} -p -D -m 0755 EL6/%{name} %{buildroot}%{_libexecdir}/%{name}
elif [[ "%{dist}" == ".el7" ]]; then
	%{__install} -p -D -m 0755 EL7/%{name} %{buildroot}%{_libexecdir}/%{name}
fi

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README COPYING ChangeLog
%config(noreplace) %{_sysconfdir}/cron.d/%{cron_job_order}%{name}
%attr(0755,root,root) %{_libexecdir}/%{name}


%changelog
* Sun Nov 9 2014 rjmco <melcugs0-contrib-default-kernel-reboot-rjmco@tux.com.pt> - 0.2-4
- Version 0.2 released for EL6 and EL7

* Sun Nov 9 2014 rjmco <melcugs0-contrib-default-kernel-reboot-rjmco@tux.com.pt> - 0.1-2
- Issue #1 fix - Package fails to install due to dependency check errors

* Sun Nov 9 2014 rjmco <melcugs0-contrib-default-kernel-reboot-rjmco@tux.com.pt> - 0.1-1
- initial package version
